import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-first-project';
  public name: string = 'Peter';
  public id: number = 1;

  public message: string = 'Hello I am From Typescript!'
  public reminders: string[] = [
    'Learning TypeScript',
    'Learning Angular',
    'Having Fun with Angular'
  ];
  
}
